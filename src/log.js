const { createLogger, format, transports } = require("winston");
const DailyRotateFile = require("winston-daily-rotate-file");

const defaultOptions = {
  datePattern: "YYYY-MM-DD",
  zippedArchive: true,
  maxSize: "20m",
  maxFiles: "14d",
};

module.exports = createLogger({
  format: format.combine(
    format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
    format.align(),
    format.printf(
      (info) => {
        const content = `[${info.level.toUpperCase()}] ${[info.timestamp]} ${info.message}`;
        console.error(123, content);
        return content;
      }

        
    )
  ),
  transports: [
    new DailyRotateFile({
      filename: "logs/info-%DATE%.log",
      level: "info",
      format: format.combine(
        format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
        format.align(),
        format.printf(
          (info) =>
            info.level === 'info' ? `[${info.level.toUpperCase()}] ${[info.timestamp]} ${info.message}`:''
        )
      ),
      ...defaultOptions,
    }),

    new DailyRotateFile({
      filename: "logs/error-%DATE%.log",
      level: "error",
    }),
  ],
});